package phraseoftheday.timoteojorge.com.br.phraseoftheday;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private TextView textNewPhrase;
    private Button newPhraseButton;
    private String[] phrases = {
            "If you want to achieve greatness stop asking for permission.",
            "Things work out best for those who make the best of how things work out.",
            "To live a creative life, we must lose our fear of being wrong.",
            "All our dreams can come true if we have the courage to pursue them.",
            "Good things come to people who wait, but better things come to those who go out and get them.",
            "If you do what you always did, you will get what you always got.",
            "Opportunities don’t happen, you create them.",
            "Try not to become a person of success, but rather try to become a person of value.",
            "Great minds discuss ideas; average minds discuss events; small minds discuss people.",
            "If you don’t value your time, neither will others. Stop giving away your time and talents- start charging for it. ",
            "A successful man is one who can lay a firm foundation with the bricks others have thrown at him."
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textNewPhrase = (TextView) findViewById(R.id.text_new_phrase_id);
        newPhraseButton = (Button) findViewById(R.id.button_new_phrase_id);

        //textNewPhrase.setText(phrases[0]);

        newPhraseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Random random = new Random();
                int randomNumber = random.nextInt(phrases.length);
                textNewPhrase.setText(phrases[randomNumber]);
            }
        });
    }
}
